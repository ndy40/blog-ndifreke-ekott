/* ==============================================
    Fixed Navbar
   =============================================== */
jQuery(window).bind('scroll', function (){
  if (jQuery(window).scrollTop() > 80){
    jQuery('.header').addClass('fixed-nav');
  } else {
    jQuery('.header').removeClass('fixed-nav');
  }
});

$( document ).ready(function() {
  'use strict';
      $('.grid').masonry({
        // options
      });
});

/* ======================================
     Full Screen Header
   ====================================== */
    function SetResizeContent() {
        var minheight = $(window).height();
        $(".full-screen").css('min-height', minheight);
    }
    SetResizeContent();
    //Navigation Visible On Scroll


    var IS_IPAD = navigator.userAgent.match(/iPad/i) != null;
    var IS_IPHONE = (navigator.userAgent.match(/iPhone/i) != null) || (navigator.userAgent.match(/iPod/i) != null);


    if (IS_IPAD == true || IS_IPHONE == true) {

    }
    else
    {
        $('li.content-scroll figure').removeAttr('class');

        try {
            $(".content-scroll").mCustomScrollbar({
                autoHideScrollbar: true,
                theme: "minimal-dark"
            });
        }
        catch (err) {
        }

    }

/**
 *
 * TODO: When we have enough contents, we can add Google search again.
 *
var displayResults = function (data) {
    if (data.searchInformation.totalResults > 0) {
        let searchModel = document.getElementById("search-modal");
        searchModel.querySelector("#myModalLabel").textContent = "Search result(s) for-  " + data.queries.request[0].searchTerms;
        var items = [];

        data.items.forEach(function (item) {
            var div = '<section class="row"><div class="search-result-item col">'
            if (item.pagemap.hasOwnProperty('cse_thumbnail')) {
                div += '<img src="' + item.pagemap.cse_thumbnail.src + '" class="img-thumbnail pull-left" width="120" style="margin-right: 10px;"/>';
            }
            div += '<div class="search-result-text">'
            div += '<span class="title"><a href="' + item.link + '">'  + item.title + '</a></span>';
            div += '<span class="summary">' + item.snippet + '</span></div></div></section>';

            items.push(div);
        });

        document.getElementById('result-content').innerHTML = items.join('');

        $(searchModel).modal('show');
    }
};

var cx = '017219499815181731390:mlfevamn7sa';
var key = 'AIzaSyBYRZycrWKqVw06IU3cs__steNn8L-zGqA';
var requestUrl = 'https://www.googleapis.com/customsearch/v1?cx=' + cx + "&key=" + key;

$("#search-btn").on('click',  function (e) {
    $.ajax(requestUrl, {
        "cache": true,
        "method": "GET",
        "crossDomain": true,
        "dataType": "jsonp",
        "jsonpCallback": "displayResults",
        "data": {
            "q" : document.getElementById("search").value
        }
    });

});

 */