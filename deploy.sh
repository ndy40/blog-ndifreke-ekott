#!/usr/bin/env bash

echo "Deploying website: https://ndifreke-ekott.info"

if [[ ! -d "/home/deploy/ndifreke-ekott-info/" ]]; then
    echo "Directory **ndifreke-ekott-info** does not exists"
    mkdir ndifreke-ekott-info
    cd ndifreke-ekott-info
    git clone git@bitbucket.org:ndy40/blog-ndifreke-ekott.git .

    cecho "Setup Variables"
    cp $HOME/etc/blog/.env .env
fi

cd $HOME/ndifreke-ekott-info
echo "CURRENT DIR: $PWD"

echo "Check Status of running blog"
container_status=$(docker inspect -f '{{.State.Running}}' blog)

if [[ true = "$container_status" ]]; then
    echo "Container running. Shutting down for deployment"
    docker-compose down

    if [[ "$?" = 1 ]]; then
        echo "Error occured while shutting down containers"
        exit 1
    fi
fi

echo "Staring up project"
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

if [[ "$?" = 1 ]]; then
    echo "An error occurred starting up project"
    exit 1
fi

echo "Start Up Stats"
container_stats=$(docker inspect  blog)

CONTAINER_ID=$(echo $container_stats | jq '.[0].Id')
CONTAINER_CREATED=$(echo $container_stats | jq '.[0].Created')
CONTAINER_STATUS=$(echo $container_stats | jq '.[0].State.Status')

echo Container ID: $CONTAINER_ID
echo Created At: $CONTAINER_CREATED
echo Status: $CONTAINER_STATUS

exit 0